#!/usr/bin/env bash
#=============================#
#   Automatic installation    #
#   of programs on new macs   #
# REMOVE WHAT YOU DO NOT NEED #
#=============================#

TEMP_DIR=$(mktemp -d)
HOMEBREW_INSTALLER_URL="https://raw.githubusercontent.com/Homebrew/install/master/install"
COLOR_SCHEME_URL="https://raw.githubusercontent.com/MartinSeeler/iterm2-material-design/master/material-design-colors.itermcolors"
 
set -e
trap on_sigterm SIGKILL SIGTERM

RESET_COLOR="\033[0m"
RED_COLOR="\033[0;31m"
GREEN_COLOR="\033[0;32m"
BLUE_COLOR="\033[0;34m"

function reset_color() {
    echo -e "${RESET_COLOR}\c"
}

function red_color() {
    echo -e "${RED_COLOR}\c"
}

function green_color() {
    echo -e "${GREEN_COLOR}\c"
}

function blue_color() {
    echo -e "${BLUE_COLOR}\c"
}

function separator() {
    green_color
    echo "#=============================STEP FINISHED=============================#"
    reset_color
}

function introduction() {
    green_color     
    cat << "EOF"
                        .__                 __         .__  .__                
  _____ _____    ____   |__| ____   _______/  |______  |  | |  |   ___________ 
 /     \\__  \ _/ ___\  |  |/    \ /  ___/\   __\__  \ |  | |  | _/ __ \_  __ \
|  Y Y  \/ __ \\  \___  |  |   |  \\___ \  |  |  / __ \|  |_|  |_\  ___/|  | \/
|__|_|  (____  /\___  > |__|___|  /____  > |__| (____  /____/____/\___  >__|   
      \/     \/     \/          \/     \/            \/               \/       
EOF

    reset_color
    sleep 1
}
# Install Command Line Tools for Xcode
function install_command_line_tools() {
    blue_color
    echo "Trying to detect installed Command Line Tools..."

    if ! [ $(xcode-select -p) ]; then
        blue_color
        echo "You don't have Command Line Tools installed"
        echo "They are required to proceed with installation"

        green_color
        read -p "Do you agree to install Command Line Tools? (y/N) " -n 1 answer
        echo
        if [ ${answer} != "y" ]; then
            exit 1
        fi

        blue_color
        echo "Installing Command Line Tools..."
        echo "Please, wait until Command Line Tools will be installed, before continue"
        echo "I can't wait for its installation from the script, so continue..."

        xcode-select --install
    else
        blue_color
        echo "Seems like you have installed Command Line Tools, so skipping..."
    fi

    reset_color
    separator
    sleep 1
}

# Install Homebrew package management
function install_homebrew() {
    blue_color
    echo "Trying to detect installed Homebrew..."

    if ! [ $(which brew) ]; then
        blue_color
        echo "Seems like you don't have Homebrew installed"
        echo "Homebrew is needed to install relevant packages. Accepting this will install the development suite"

        green_color
        read -p "Do you agree to proceed with Homebrew installation? (y/N) " -n 1 answer
        echo
        if [ ${answer} != "y" ]; then
            exit 1
        fi

        blue_color
        echo "Installing Homebrew..."

        ruby -e "$(curl -fsSL ${HOMEBREW_INSTALLER_URL})"
        brew update
        brew upgrade
        echo "Installing Cask"
        brew tap caskroom/cask
        green_color
        echo "Homebrew installed!"
    else
        blue_color
        echo "You already have Homebrew installed, so skipping..."
    fi

    reset_color
    separator
    sleep 1
}

# Installing iTerm2
function install_iTerm2() {
    blue_color
    echo "Trying to find installed iTerm..."

    if ! [ $(ls /Applications/ | grep iTerm.app) ]; then
        blue_color
        echo "I can't find installed iTerm"

        green_color
        read -p "Do you agree to install it? (y/N) " -n 1 answer
        echo
        if [ ${answer} != "y" ]; then
            exit 1
        fi

        blue_color
        echo "Installing iTerm2..."

        brew cask install iterm2

        green_color
        echo "iTerm2 installed!"
    else
        blue_color
        echo "Found installed iTerm.app, so skipping..."
    fi

    reset_color
    separator
    sleep 1
}

# Installing iTerm2 color scheme
function install_color_scheme() {
    green_color
    read -p "Do you want to install color scheme for iTerm? (y/N) " -n 1 answer
    echo
    if [[ ${answer} == "y" || ${answer} == "Y" ]]; then
        blue_color
        echo "Downloading color scheme in ${TEMP_DIR}..."

        cd ${TEMP_DIR}
        curl -fsSL ${COLOR_SCHEME_URL} > ./material-design.itermcolors

        blue_color
        echo "iTerm will be opened in 5 seconds, asking to import color scheme (in case, you installed iTerm)"
        echo "Close iTerm when color scheme will be imported"
        sleep 5
        open -W ./material-design.itermcolors

        green_color
        echo "Color Scheme is installed!"
    else
        blue_color
        echo "Skipping Color Scheme installation..."
    fi

    reset_color
    separator
    sleep 1
}

#I nstalling Development tools
function install_software() {
  green_color
  echo "Installing Development Suite"
  echo
  blue_color
  # Browsers
  brew cask install google-chrome firefox

  # Code editor
  brew cask install visual-studio-code atom
 
  # Communication
  brew cask install slack
 
  # Productivity
  brew cask install microsoft-office evernote dropbox filezilla tunnelblick vlc the-unarchiver virtualbox docker java vagrant

  brew install git terraform groovy go bash-git-prompt

  # Enable Git prompt
    cat << EOF >> ~/.bash_profile
if [ -f "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh" ]; then
  __GIT_PROMPT_DIR=$(brew --prefix)/opt/bash-git-prompt/share
  source "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh"
fi
EOF
 
  # Extra
  brew cask install spotify

  reset_color
  separator
  sleep 1
}

function configure_vsc() {
    green_color
    echo "Adding Code to $PATH"
    cat << EOF >> ~/.bash_profile
# Add Visual Studio Code (code)    
export PATH="\$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
EOF

    touch ~/.bash_profile
    if [ $(which code) ]; then
        echo "Installing VSC Extensions"
        blue_color
        code --install-extension ms-vscode.csharp --install-extension msjsdiag --install-extension peterjausovec.vscode-docker \
        --install-extension ms-vscode.go --install-extension ecmel.vscode-html-css --install-extension ms-vscode.powershell \
        --install-extension ms-python.python --install-extension humao.rest-client --install-extension rebornix.ruby \
        --install-extension ms-mssql.mssql --install-extension mauve.terraform --install-extension erd0s.terraform-autocomplete \
        --install-extension dotjoshjohnson.xml --install-extension redhat.vscode-yaml --force
    else
        green_color
        echo "code cli not enabled skipping..."
    fi
    reset_color
    separator
    sleep 1
}

# Post_install check
function post_install() {
  green_color
  echo
  echo "Setup was successfully done"
  echo "Do not forgot to make a few simple, but manual things:"
  echo
  echo "1) Open iTerm -> Preferences -> Profiles -> Colors -> Presets and apply color preset"
  echo "2) Open iTerm -> Preferences -> Profiles -> Text -> Font and apply font (for non-ASCII as well)"
  echo "3) Set the GOBIN and GOPATH environment variables relative to your go directory"
  echo
  echo "Here is a list of programs that you may need and that we didn't install automatically:"
  echo "Microsoft Remote Desktop"
  echo "MSG Viewer"
  echo "Project Viewer 365"
  echo "Groovy console"
  echo

  reset_color
}

# sigterm error
function on_sigterm() {
  red_color
  echo
  echo "Unexpected error. Please attempt a manual install"
  reset_color
  exit 1
}

# main
introduction
install_command_line_tools
install_homebrew
install_iTerm2
install_color_scheme
install_software
configure_vsc
post_install